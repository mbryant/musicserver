{
  config,
  pkgs,
  lib,
  rustPlatform,
  ...
}:

{
  imports = [
    # Include the results of the hardware scan.
    /etc/nixos/hardware-configuration.nix
  ];

  # Nix options
  system.stateVersion = "23.11";
  system.autoUpgrade.channel = "https://nixos.org/channels/nixos-24.11";
  nixpkgs.config.allowUnfree = true;

  #############################################################################
  # General Machine Configuration
  #############################################################################
  boot = {
    # TODO: What kernel works?
    # kernelPackages = pkgs.linuxPackages_latest;
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
  };

  networking = {
    hostName = "musicserver";
    nameservers = [ "1.1.1.1#one.one.one.one" ];
    networkmanager = {
      enable = true;
      dns = "systemd-resolved";
      wifi = {
        backend = "iwd";
      };
    };
    firewall = {
      enable = true;
      allowedTCPPorts = [
        # Musicserver frontend
        443
      ];
    };
  };

  hardware.pulseaudio = {
    enable = true;
    package = pkgs.pulseaudioFull;
    systemWide = true;
  };

  services = {
    openssh = {
      enable = true;
      openFirewall = true;
      settings = {
        # Only public key allowed
        PasswordAuthentication = false;
      };
    };
    xserver.xkb = {
      layout = "us";
      variant = "";
    };
    resolved = {
      enable = true;
      fallbackDns = [ "1.1.1.1#one.one.one.one" ];
      extraConfig = ''
        DNSOverTLS=yes
      '';
    };
    nginx = {
      enable = true;
      virtualHosts."music.programsareproofs.com" = {
        # Nginx handles TLS, then routes everything to the local musicserver backend
        forceSSL = true;
        useACMEHost = "music.programsareproofs.com";

        # The websocket is on a different port from everything else
        locations = {
          "/socket" = {
            proxyPass = "http://localhost:6600";
            proxyWebsockets = true;
          };
          "/" = {
            proxyPass = "http://localhost:4043";
            extraConfig = ''
              proxy_http_version 1.1;
              proxy_request_buffering on;
            '';
          };
        };
      };
    };
    tailscale = {
      enable = true;
    };
  };

  users.users.musicserver = {
    isNormalUser = true;
    description = "musicserver";
    extraGroups = [
      "networkmanager"
      "wheel"
      "audio"
      "pulse-access"
    ];
    packages = [ ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHmqpOBlYDOnhyZf9sbLi6BMdpkt50Ul5sgMim5XApiK"
    ];
  };
  users.defaultUserShell = pkgs.fish;

  security = {
    # Don't require a password for sudo, since it's not a shared system.
    sudo.wheelNeedsPassword = false;

    acme = {
      defaults.email = "letsencrypt@programsareproofs.com";
      acceptTerms = true;

      # Automatically generate certificates to allow the musicserver to host itself
      certs."music.programsareproofs.com" = {
        dnsProvider = "cloudflare";
        # Contains a single line with the format "CLOUDFLARE_DNS_API_TOKEN=$TOKEN"
        credentialsFile = "/etc/cloudflare-creds.ini";
        # Reload nginx whenever we get new certs
        reloadServices = [ "nginx.service" ];
        # Allow nginx to read the certs
        group = "nginx";
      };
    };
  };

  #############################################################################
  # Application Configuration
  #############################################################################
  fileSystems."/music" = {
    device = "192.168.1.69:/mnt/8tb/music";
    fsType = "nfs";
    options = [
      "ro"
      "async" # Do all I/O asynchronously since we'll have a lot of parallelism
      "noexec" # We don't run any binaries
      "soft" # Report an error if NFS disappears
      "nolock" # File locking doesn't matter since we only ever read
      "noatime" # Don't write access times
      "intr" # Allow NFS requests to be interrupted
      "ac" # Cache file attributes, since they never change
    ];
  };

  environment.systemPackages = with pkgs; [
    alsa-utils
    cargo
    curl
    fd
    fzf
    gcc
    git
    hexyl
    htop
    mplayer
    nixfmt-rfc-style
    ripgrep
  ];
  environment.shells = [
    pkgs.fish
    pkgs.zsh
  ];

  programs = {
    neovim = {
      enable = true;
      vimAlias = true;
      defaultEditor = true;

      configure = {
        customRC = ''
          set autoindent
          set expandtab
          set ignorecase
          set incsearch
          set mouse=
          set shiftwidth=4
          set smartcase
          set softtabstop=4
          set tabstop=4
          colorscheme habamax
        '';
      };
    };
    fish = {
      enable = true;
    };
    zsh = {
      enable = true;
    };
  };
  systemd.services.musicserver =
    let
      repo = "${config.users.users.musicserver.home}/musicserver";
      musicserver = pkgs.rustPlatform.buildRustPackage rec {
        pname = "musicserver";
        version = "0.4.0";

        src = lib.cleanSource repo;
        cargoLock = {
          lockFile = ./Cargo.lock;
        };
        meta.mainProgram = "musicserver";
      };
    in
    {
      enable = true;
      description = "Music server";
      path = with pkgs; [
        mplayer
        alsa-utils
      ];
      serviceConfig = {
        Type = "simple";
        User = "musicserver";
        Group = "pulse-access";

        Environment = [
          "SONG_DIRECTORY=/music"
          "RUST_LOG=info"
        ];
        WorkingDirectory = "${config.users.users.musicserver.home}";
        ExecStart = lib.getExe musicserver;

        Restart = "on-failure";
        RestartSec = "1s";
      };

      after = [
        # No sense in running until we're connected to the music
        "music.mount"
      ];

      wantedBy = [ "multi-user.target" ];
    };
}
