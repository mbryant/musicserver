var Random = new function() {
	// Return an int within [min, max)
	this.nextInt = function(min, max) {
		return Math.floor(Math.random() * (max - min)) + min;
	};
	// Return a float within [min, max]
	this.nextFloat = function(min, max) {
		return Math.random() * (max - min + 1) + min;
	};
	// Return a bool with p probability of being true
	this.nextBool = function(p) {
		return Math.random() < p;
	};
};

function formatSeconds(time) {
	var seconds = Math.floor(time);

	if (seconds < 0 || seconds >= 3600) {
		return "???";
	}

	var minutes = Math.floor(seconds / 60);
	seconds %= 60;

	return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
}

var State =  new function() {
	this.songs = [];
	this.queue = [];

	this.volume   = 100;
	this.playing  = false;
	this.current  = null;
	this.position = 0;

	var search = "";
	var all   = [];
	var lookup = {};

	this.swapPlaying = function() {
		if (this.playing) {
			Backend.pause();
		} else {
			Backend.resume();
		}
		this.playing = !this.playing;
		UI.PlayerTab.play(this.playing);
	};

	this.setPlaying = function(playing) {
		this.playing = playing;
		UI.PlayerTab.play(playing);
	}

	this.play = function(song_id) {
		let song = lookup[song_id];
		if (song == this.current) {
			return;
		}

		console.info("Playing", "'" + song.title + "'", "by", "'" + song.artist + "'");
		this.current = song;

		MusicHistory.addHistory(song);
		UI.playSong(song);
	};

	this.setVolume = function(level) {
		if (level < 0 || level > 100) {
			return;
		}

		this.volume = level;
		Backend.volume(level);
	};

	this.updateVolume = function(level) {
		if (level < 0 || level > 100) {
			return;
		}

		UI.PlayerTab.volume(level);
		this.volume = level;
	};

	this.gotoPosition = function(position) {
		if (position < 0 || position > this.current.duration) {
			return;
		}

		Backend.position(position);
		this.position = position;
	};

	this.updatePosition = function(position) {
		if (position < 0 || position > this.current.duration) {
			return;
		}

		UI.PlayerTab.position(position);
		this.position = position;
	};

	this.updateDuration = function(song_id, duration) {
		if (duration === 0) {
			// This isn't a real update - ignore it.
			return;
		}

		let song = lookup[song_id];

		if (song.duration === 0) {
			song.duration = duration;
		} else if  (song.duration != duration) {
			console.warn("Duration for " + song +
					" doesn't match new duration of " + duration);
		}
	}

	this.initialize = function(song_array) {
		// We're given an array of songs, which we want to convert to a table
		// of artist -> [songs].
		artists = {};

		for (var i = 0; i < song_array.length; i++) {
			var song = song_array[i];
			song.id = i;

			song.artist = song.artist || "Unknown Artist";

			if (song.artist in artists) {
				artists[song.artist].push(song);
			} else {
				artists[song.artist] = [song];
			}
		}

		var sorted = [];
		var names = Object.keys(artists).sort();
		names.forEach(function(name) {
			var songs = artists[name].sort(function(a,b) {
				if (a.title < b.title) {
					return -1;
				}
				if (a.title > b.title) {
					return 1;
				}
				return 0;
			});

			for (var i = 0; i < songs.length; i++) {
				var song = songs[i];

				song.title = song.title || "Unknown";
				song.album = song.album || "Unknown Album";
			}

			sorted.push({name: name, songs: songs});
		});

		this.songs = sorted;
		all = this.songs;
		lookup = song_array;
	};

	this.filter = function(query) {
		if (query === search) {
			return;
		}

		// If the new query is a subset of the old query, we don't need to
		// filter the entire list again.
		var filtering  = (query.includes(search) ? this.songs : all);
		var subqueries = query.toLowerCase().split(" ");

		// We can't do a filter because we may be keeping portions of existing
		// objects.  As such, we need to map to get new objects, then filter
		// out the ones to ignore.
		this.songs = filtering.map(function(artist) {
			var lower = artist.name.toLowerCase();
			var remaining = subqueries.filter(function(search) {
				return !lower.includes(search);
			});

			// The artist matched all subqueries
			if (remaining.length === 0) {
				return artist;
			}

			// The artist matched some (possibly zero) portion of the search,
			// and we're now trying to find the songs that match the rest.
			var songs = artist.songs.filter(function(song) {
				let lower = song.title.toLowerCase();
				let album = song.album.toLowerCase();
				let matched = remaining.filter(function(search) {
					return !(lower.includes(search) || album.includes(search));
				});

				// Keep the song if it matched the rest of the search criteria.
				return (matched.length === 0);
			});

			if (songs.length === 0) {
				return null;
			}

			var filteredArtist = Object.assign({}, artist);
			filteredArtist.songs = songs;

			return filteredArtist;
		}).filter(function(x) { return x !== null; });

		UI.MusicTab.filter();
		search = query;
	};

	// TODO: this is rather messy
	this.updateQueue = function(ids) {
		if (ids.length === this.queue.length &&
				this.queue.every((x, i) => x == ids[i])) {
			// Queue hasn't actually changed.
			return;
		}

		this.queue = [];

		for (var i = 0; i < ids.length; i++) {
			var song = lookup[ids[i]];

			if (song !== undefined) {
				this.queue.push(song);
			}
		}

		UI.QueueTab.redraw();
	};

	this.appendQueue = function(song) {
		this.queue.push(song);
		Backend.enqueue(song);
	};

	this.removeQueue = function(song) {
		var index = this.queue.indexOf(song);

		if (index !== -1) {
			this.queue.splice(index, 1);
		}

		// TODO: Add removal support to the backend
	};
};

// UI class
var UI = new function() {
	// Dom

	// Tabs
	let _playerTab     = document.getElementById("player");
	let _queueTab      = document.getElementById("queue");
	let _listTab       = document.getElementById("musiclist");
	let _loadingTab    = document.getElementById("loading");
	let _deniedTab     = document.getElementById("denied");
	let _disconnectTab = document.getElementById("disconnect");
	let _bootingTab    = document.getElementById("booting");

	// Locals
	let filtered     = [];
	let visibleNodes = [];
	let currentSong  = null;
	let currentTab   = _loadingTab;
	let parent = this;

	this.PlayerTab = new function() {
		var _title  = document.getElementById("title");
		var _artist = document.getElementById("artist");
		var _timeElapsed   = document.getElementById("time-elapsed");
		var _position      = document.getElementById("position");
		var _timeRemaining = document.getElementById("time-remaining");
		var _volumeSlider  = document.getElementById("volume");
		var _volumeLevel   = document.getElementById("volume-level");
		var _play = document.getElementById("play");
		var _coverart = document.getElementById("coverart");

		// TODO: This whole chunk should be abstracted out into a
		// "SliderListener event" or something like that.
		var updatingPosition = false;
		var updatingVolume   = false;

		this.initialize = function() {
			// Attach the slider's listener for timestamp changes
			_position.onfocus = function() {
				updatingPosition = true;
			}.bind(this);

			// Just in case we don't change it
			_position.onblur = function() {
				updatingPosition = false;
			}.bind(this);

			_position.onchange = function() {
				State.gotoPosition(_position.value);
				_position.blur();
			}.bind(this);

			// Attach a listener for volume changes
			_volumeSlider.onfocus = function() {
				updatingVolume = true;
			}.bind(this);

			// Just in case we don't change it
			_volumeSlider.onblur = function() {
				updatingVolume = false;
			}.bind(this);

			_volumeSlider.onchange = function() {
				State.setVolume(_volumeSlider.value);
				_volumeSlider.blur();
			}.bind(this);
		};

		this.song = function(song) {
			// Set the title and artist name
			_artist.innerHTML = song.artist;
			_title.innerHTML = song.title;
			document.title = _artist.textContent + " | " + _title.textContent;

			// Display the cover art
			_coverart.style.backgroundImage = `url(/cover_art/${song.id})`;

			// Set the slider values properly.  In particular, we keep the range
			// non-zero so the slider looks reasonable.
			_position.value = 0;
			_position.min = 0;
			_position.max = (song.duration !== 0 ? song.duration : 1);
			_timeRemaining.innerText = formatSeconds(song.duration);
		};

		this.position = function(position) {
			_timeElapsed.innerText = formatSeconds(position);
			_timeRemaining.innerText = formatSeconds(State.current.duration - position);

			if (!updatingPosition) {
				_position.value = position;

				if (_position.max != State.current.duration) {
					_position.max = State.current.duration;
				}
			}
		};

		this.volume = (level) => {
			_volumeLevel.innerText = level;

			if (!updatingVolume) {
				_volumeSlider.value = level;
			}
		};

		// Set the play icon to the appropriate play/stop position depending on bool
		this.play = function(playing) {
			_play.innerHTML = playing ? "Pause" : "Play";
		};

		this.playButton = function() {
			if (State.current === null) {
				// Playing when there's nothing currently playing should start
				// a new song.
				Backend.next();
			} else {
				State.swapPlaying();
			}
		};

		this.nextButton = function() {
			var next = MusicHistory.getNext();

			if (next !== null) {
				Backend.play(next);
			} else {
				Backend.next();
			}
		};

		this.prevButton = function() {
			// Do nothing if we haven't played a song yet
			if (State.current === null) {
				return;
			}

			// Rewind if we are playing and didn't just start
			if (State.playing && State.position > 5) {
				State.gotoPosition(0);
				return;
			}

			// Rewind if there is no previous song.
			var prev = MusicHistory.getPrev();
			if (prev === null) {
				State.gotoPosition(0);
				return;
			}

			Backend.play(prev);
		};
	};

	this.MusicTab = new function() {
		var _search   = document.getElementById("search");
		var _playlist = document.getElementById("playlist");
		var _songs    = document.getElementById("songs");

		this.initialize = function() {
			_playlist.onscroll = function() {
				this.render();
			}.bind(this);

			// Create nodes for the full list of songs.
			for (var i = 0; i < State.songs.length; i++) {
				var artist = State.songs[i];

				var artistNode = document.createElement('dl');
				artistNode.innerHTML = "<dt>" + artist.name + "</dt>";
				artistNode.style.display = "none";

				artist.node = artistNode;
				artist.offset = 0;
				artist.height = 0;

				for (var j = 0; j < artist.songs.length; j++) {
					var song = artist.songs[j];
					var songNode = document.createElement('dd');
					song.node = songNode;
					song.artistNode = artistNode;

					var queueNode = document.createElement('span');
					queueNode.className = "queueStatus";
					queueNode.onclick = (function(song) {
						return function(event) {
							UI.QueueTab.handle(song);
							event.stopPropagation();
						};
					})(song);

					songNode.innerHTML = '<span class="title">' + song.title + '</span>' +
										 '<span class="album">' + song.album + '</span>';
					songNode.insertBefore(queueNode, songNode.firstChild);
					songNode.style.display = "none";

					songNode.onclick = (function(song) {
						return function() { Backend.play(song); }
					}) (song);

					artistNode.appendChild(songNode);
				}
			}

			_search.value = "";
			this.filter();
		};

		// Create a list of the drawable songs
		this.filter = function() {
			var artists = State.songs;

			// Clear the playlist and hide the set of songs that were previously visible.
			_songs.innerHTML = "";

			filtered.forEach(function(artist) {
				artist.songs.forEach(function(song) {
					song.node.style.display = "none";
				});
			});

			// We will invalidate the set of visible nodes.
			filtered = [];
			visibleNodes = [];

			if (artists.length === 0) {
				this.render();
				return;
			}

			// Compute the size of a single song + artist
			var songHeight = 51;  // Extra pixel for the border
			var artistHeight = 40;

			// Read all the sizes and render them onto a fragment to prevent reflows.
			var currentOffset = 0;
			var fragment = document.createDocumentFragment();
			for (var i = 0; i < artists.length; i++) {
				var artist = artists[i];

				artist.offset = currentOffset,
					artist.height = artistHeight + artist.songs.length * songHeight

						for (var j = 0; j < artist.songs.length; j++) {
							var song = artist.songs[j];
							song.node.style.display = "";
						}

				filtered.push(artist);
				currentOffset += artist.height;

				artist.node.style.top = artist.offset + "px";
				fragment.appendChild(artist.node);
			}

			_songs.style.height = currentOffset + "px";
			_songs.appendChild(fragment);

			this.render();
		};

		this.render = function() {
			var displayTop = _playlist.scrollTop;
			var displayBottom = displayTop + window.outerHeight;

			visibleNodes.filter(function(node) {
				// It's either above or below us in the list.
				if (node.offsetTop + node.offsetHeight < displayTop ||
					node.offsetTop > displayBottom) {

						node.style.display = "none";
						return false;
					}

				return true;
			});

			for (var i = 0; i < filtered.length; i++) {
				var artist = filtered[i];

				// Search for the first node that we need to display.  A linear
				// search is probably good enough in this case.
				// TODO: if this becomes an issue, some sort of index should be
				// added.
				if (artist.offset + artist.height < displayTop) {
					continue;
				}

				// We're at the end of the nodes that might be displayable.
				if (artist.offset > displayBottom) {
					break;
				}

				// If it's visible right now, we're already tracking it.
				if (artist.node.style.display === "none") {
					artist.node.style.display = "";
					visibleNodes.push(artist.node);
				}
			}
		};

		this.queuedState = function(song, state) {
			song.node.firstChild.innerText = state;
		};

		this.search = function() {
			State.filter(_search.value);
		};

		this.focus = function(song) {
			console.warn("Focused on", song, "but it didn't work!");
			song.artistNode.style.display = "";
			visibleNodes.push(song.artistNode);

			song.node.scrollIntoView();  // TODO: this doesn't work when the playlist is out of view
			_playlist.scrollTop -= song.artistNode.firstChild.offsetHeight;

			// Remove the highlight from the previously focused song and add it to the new one.
			if (currentSong !== null) {
				currentSong.className = "";
			}

			currentSong = song.node;
			currentSong.className = "selected";
		};
	};

	this.QueueTab = new function() {
		var _queue = document.getElementById("queue-list");
		var drawn = [];

		// NOTE: must be initialized after the music list
		this.initialize = function() {
			for (var i = 0; i < State.songs.length; i++) {
				var artist = State.songs[i];

				for (var j = 0; j < artist.songs.length; j++) {
					var song = artist.songs[j];

					song.enqueued = false;
					parent.MusicTab.queuedState(song, "Q");
				}
			}
		};

		var insert = function(song, target) {
			// Add a song to the end of the given target node
			var songNode = document.createElement("dd");

			songNode.innerHTML = '<span class="title">' + song.title + '</span>' +
				'<span class="album">' + song.artist + '</span>';

			target.appendChild(songNode);
		};

		this.redraw = function() {
			// Clear the existing queue members
			for (var i = 0; i < drawn.length; i++) {
				var song = drawn[i];

				song.enqueued = false;
				parent.MusicTab.queuedState(drawn[i], "Q");
			}

			drawn = [];

			// Redraw the queue state on the music tab
			for (var i = 0; i < State.queue.length; i++) {
				var song = State.queue[i];

				song.enqueued = true;
				parent.MusicTab.queuedState(song, i + 1);
				drawn.push(song);
			}

			// Redraw the list on the queue tab
			_queue.innerHTML = "";
			var list = document.createDocumentFragment();

			for (var i = 0; i < drawn.length; i++) {
				insert(drawn[i], list);
			}

			_queue.appendChild(list);
		};

		this.handle = function(song) {
			if (song.enqueued) {
				State.removeQueue(song);
				song.enqueued = false;
				parent.MusicTab.queuedState(song, "Q");

				// TODO: this is somewhat inefficient
				this.redraw();
			} else {
				State.appendQueue(song);
				drawn.push(song);

				song.enqueued = true;
				parent.MusicTab.queuedState(song, drawn.length);
				insert(song, _queue);
			}
		};

		this.clear = function(backend) {
			if (State.queue.length == 0) {
				return;
			}
			if (!confirm("Clear the queue of songs?")) {
				return;
			}

			State.updateQueue([]);
			Backend.clearQueue();
		}
	};

	this.playSong = function(song) {
		this.PlayerTab.song(song);
		this.PlayerTab.play(true);
		this.MusicTab.focus(song);
	};

	this.tab = function(newTab) {
		const oldTab = currentTab;

		switch (newTab) {
			case "player":
				currentTab = _playerTab;
				break;
			case "queue":
				currentTab = _queueTab;
				break;
			case "musiclist":
				currentTab = _listTab;
				break;
			case "nopermission":
				currentTab = _deniedTab;
				break;
			case "connectionlost":
				currentTab = _disconnectTab;
				break;
			case "loading":
				currentTab = _loadingTab;
				break;
			case "booting":
				currentTab = _bootingTab;
				break;
		}

		if (oldTab != currentTab) {
			oldTab.style.display = "none";
			currentTab.style.display = "flex";

			console.debug("Switched tabs: ", oldTab.id, "->", currentTab.id);
		}
	};

	this.noPermission = function() {
		this.tab("nopermission");
	};

	this.timedOut = function() {
		this.tab("loading");
	}

	this.serverBooting = function() {
		this.tab("booting");
	}

	this.connectionLost = function() {
		this.tab("connectionlost");
	};

	this.initialize = function() {
		this.PlayerTab.initialize();
		this.MusicTab.initialize();
		this.QueueTab.initialize();

		this.tab("player");
	};
};

var MusicHistory = new function() {
	this.history = [];
	this.position = -1;

	// Return the previous song if it exists, else null if currently playing first song
	this.getPrev = function() {
		if (this.position > 0) {
			this.position -= 1;
			return this.history[this.position];
		}
		return null;
	};

	// Return the next song if it exists, else null
	this.getNext = function() {
		if (this.position < this.history.length - 1) {
			this.position += 1;
			return this.history[this.position];
		}
		return null;
	};

	// Add song located at id to the history if it's a new song.
	// If we're playing back in the list, we don't want to add it to history.
	this.addHistory = function(id) {
		if (this.position === this.history.length - 1) {
			this.history.push(id);
			this.position += 1;
		}
	};
};

var Backend = new function() {
	let socket          = null;
	let failures        = 0;
	let sequence        = 0;
	let accessible      = false;
	let requested_songs = false;
	let loaded_songs    = false;

	// We may get messages before we've successfully fetched our song list.  We
	// should hold on to them to replay after the song list arrives.
	let cached_message  = null;

	let initialize = () => {
		socket = new WebSocket(`wss://${window.location.hostname}/socket`);

		// If we manage to open the socket, mark that it's accessible.
		// If this connection drops later, we'll continue to retry.
		socket.onopen = () => {
			console.info("Socket opened");

			if (State.songs.length > 0) {
				// We must've reloaded, so redraw the page.
				UI.initialize();
			} else {
				// This is either our first time opening a socket, or we've
				// reopened it and won't get any responses to previous
				// messages.
				// In either case, we should mark that we'll need to fetch the
				// song list before we can function.
				requested_songs = false;
			}

			accessible = true;
			failures = 0;
		};

		// Try to reopen our sockets for a bit after they close
		socket.onclose = function(event) {
			console.warn("Socket closed (%d times)", failures + 1);

			if (!accessible && event.code != 1000) {
				// We can't connect to the server and never have been able to.
				UI.noPermission();
				return;
			}

			// Attempt a quick reconnect without telling the user there's
			// something wrong.  If this fails, notify the user
			if (failures > 0) {
				UI.timedOut();
			}

			// This isn't a temporary loss of connection.
			if (failures >= 8) {
				UI.connectionLost();
				return;
			}

			failures++;

			// Wait a second before trying to reinitialize.
			setTimeout(initialize, 1000);
		};

		socket.onmessage = function(msg) {
			let response = JSON.parse(msg.data);

			if (response == "initializing") {
				// Server is still booting
				UI.serverBooting();

				return;
			} else if (response.constructor === Array) {
				// This must be the songlist.
				if (State.songs.length > 0) {
					// We already have our own songlist, so ignore this new one.
					return;
				}

				// It's possible for us to get multiple copies of this message,
				// since we'll get a copy when new clients connect, so we'll
				// ignore follow-on messages.
				State.initialize(response);
				UI.initialize();

				if (cached_message != null) {
					// We have an old message we should replay.
					response = cached_message;
					cached_message = null;
				} else {
					return;
				}
			} else if (State.songs.length == 0) {
				// We received an object from the server, which means it must
				// be ready for us.
				if (!requested_songs) {
					// We don't have any songs yet, so fetch them.
					send("songlist", false);
					requested_songs = true;
				}

				// We've tried to initialize but don't have data yet, so we
				// can't do anything with the messages we're receiving.
				// Just store them for later.  This is useful if a song isn't
				// currently playing, meaning the server isn't sending frequent
				// updates.
				cached_message = response;
				return;
			}

			// TODO: We shouldn't be sending the full queue every time.
			State.updateQueue(response["queue"]);
			State.updateVolume(response["volume"]);

			// TODO: We should be storing the last response and doing a
			// delta-diff on it.  This is particularly useful for
			//  - the queue
			//  - song duration
			//  - playback position
			//  - playing state
			// Alternatively, this might be more of a job for State to manage.
			if (response["playback"] !== null) {
				// The server is playing, so we should update to it.
				var playback = response["playback"];
				State.updateDuration(playback["song"], playback["duration"]);

				State.play(playback["song"]);
				State.updatePosition(playback["position"]);
				State.setPlaying(playback["playing"]);
			}
		};
	};

	var send = function(command, stateful) {
		if (socket.readyState !== 1) {
			return;
		}

		socket.send(JSON.stringify({
			sequence: sequence,
			command: command
		}));

		// Only update our sequence after we've sent the data to prevent an
		// off-by-one error with the server's sequence.
		if (stateful) {
			sequence++;
		}
	};

	this.pause = function() {
		send("pause", true);
	};

	this.resume = function() {
		send("resume", true);
	};

	this.play = function(song) {
		send("load " + song.id, true);
	};

	this.next = function() {
		send("next", true);
	};

	this.volume = function(percent) {
		send("volume " + percent, true);
	};

	this.position = function(position) {
		send("position " + position, true);
	};

	this.enqueue = function(song) {
		send("enqueue " + song.id, true);
	};

	this.clearQueue = function() {
		send("clear", true);
	};

	// TODO: We'd like a way to restrict playback to the currently filtered
	// query.
	// I believe a reasonable way to do this would be for the queue to be able
	// to contain both individual songs and search queries.

	// Initializing before the page is loaded appears to race the HTML
	// rendering and causes our state to be incorrect.
	window.onload = initialize();
};
