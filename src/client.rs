use tokio::sync::mpsc;

use crate::database::SongIndex;

#[derive(Debug)]
pub enum Query {
    Load(SongIndex),
    Enqueue(SongIndex),
    Position(u32),
    Volume(u32),
    ClearQueue,
    Next,
    Pause,
    Resume,
    InternalUpdate,
    SongList,
}

/// Parse the query, returning the server's response to a valid query
pub async fn query_server<'a, 'b>(
    query: &'a str,
    to_server: &'b mpsc::Sender<Query>,
) -> anyhow::Result<()> {
    const POSITION: &str = "position ";
    const LOAD: &str = "load ";
    const ENQUEUE: &str = "enqueue ";
    const VOLUME: &str = "volume ";

    let command = match query.trim() {
        "songlist" => Query::SongList,
        "pause" => Query::Pause,
        "resume" => Query::Resume,
        "clear" => Query::ClearQueue,
        "next" => Query::Next,
        x if x.starts_with(VOLUME) => {
            Query::Volume(x[VOLUME.len()..].parse::<u32>().expect("Invalid volume"))
        }
        x if x.starts_with(POSITION) => Query::Position(
            x[POSITION.len()..]
                .parse::<u32>()
                .expect("Invalid position"),
        ),
        x if x.starts_with(LOAD) => {
            Query::Load(x[LOAD.len()..].parse::<SongIndex>().expect("Invalid song"))
        }
        x if x.starts_with(ENQUEUE) => Query::Enqueue(
            x[ENQUEUE.len()..]
                .parse::<SongIndex>()
                .expect("Invalid song"),
        ),
        _ => anyhow::bail!("Invalid query"),
    };

    Ok(to_server.send(command).await?)
}
