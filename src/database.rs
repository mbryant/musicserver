use std::borrow::Cow;
use std::{collections::HashMap, fs::File, io::BufWriter};

use anyhow::Context;
use futures_util::StreamExt;
use id3::frame::PictureType;
use id3::{Tag, TagLike};
use serde::{Deserialize, Serialize};
use walkdir::WalkDir;

pub type SongIndex = usize;

#[derive(Serialize, Deserialize, Clone)]
pub struct SongInfo {
    /// The song's title
    title: Option<String>,
    /// The artist of the song
    artist: Option<String>,
    /// The album this song is on
    album: Option<String>,
    /// True if this song has album art
    cover_art: bool,
    /// The duration in seconds of the song
    pub duration: u32,
}

pub struct CoverArt {
    pub mime_type: String,
    pub data: Vec<u8>,
}

// A database for looking up/working with songs and paths.  This should never change after
// initialization.
#[derive(Serialize, Deserialize)]
pub struct SongDatabase {
    // The backing array of songs.
    songs: Vec<SongInfo>,

    // A lookup table from song -> path.
    paths: HashMap<SongIndex, String>,
}

impl SongDatabase {
    pub async fn new() -> SongDatabase {
        const DB_PATH: &str = "musicserver.db";

        // Scan the filesystem for all files to create a list of songs.
        let root = std::env::var("SONG_DIRECTORY")
            .expect("SONG_DIRECTORY must be set to the root of your music folder");

        // Track which songs have cached info.  If this set has entries left once we're done
        // loading the database, then we've loaded stale entries.
        let mut cache = HashMap::new();

        // Load the old database if it exists, otherwise create a new one.
        if let Ok(old_db) = tokio::fs::read(DB_PATH)
            .await
            .context("Failed to find cached db")
            .and_then(|old_db| {
                bincode::deserialize_from::<_, SongDatabase>(old_db.as_slice())
                    .context("Failed to deserialize db")
            })
        {
            // Determine the info for songs that are already cached
            for (&i, path) in old_db.paths.iter() {
                cache.insert(path.to_string(), old_db.songs[i].clone());
            }

            log::info!(
                "Loaded existing DB from {} containing {} songs",
                DB_PATH,
                cache.len()
            );
        };

        let start_time = std::time::Instant::now();

        let song_infos = futures::stream::iter(
            WalkDir::new(root)
                .into_iter()
                .filter_map(|e| e.ok())
                .filter(|e| {
                    e.file_type().is_file()
                        && e.path().extension().map_or(false, |ext| ext == "mp3")
                }),
        )
        .map(|entry| {
            let path = entry
                .path()
                .to_str()
                .context("Path can't be represented with utf-8")
                .expect("All paths should be sane")
                .to_string();

            let cached_info = cache.get(&path);

            async move {
                match cached_info {
                    Some(info) => (Cow::Borrowed(info), path),
                    _ => {
                        Tag::async_read_from_path(entry.path())
                            .await
                            .with_context(|| format!("Failed to parse ID3 tag on {}", path))
                            .map(|tag| {
                                let song = SongInfo {
                                    title: tag.title().map(String::from),
                                    artist: tag.artist().map(String::from),
                                    album: tag.album().map(String::from),
                                    cover_art: tag
                                        .pictures()
                                        .filter(|picture| {
                                            picture.picture_type == PictureType::CoverFront
                                        })
                                        .count()
                                        > 0,
                                    // The duration tag is notoriously inaccurate, so we just ignore it and
                                    // fetch the proper data when we play the songs, since the client doesn't
                                    // need to know it up-front.
                                    duration: 0,
                                };

                                log::warn!("New song entry: {}", path);
                                (Cow::Owned(song), path)
                            })
                            // TODO: We should be able to warn about this rather than crash.
                            .expect("Failed to load song information")
                    }
                }
            }
        })
        .buffer_unordered(64)
        .collect::<Vec<_>>()
        .await;

        let db: SongDatabase = {
            let mut paths = HashMap::new();
            let songs = song_infos
                .into_iter()
                .enumerate()
                .map(|(i, (song, path))| {
                    paths.insert(i, path);

                    song.into_owned()
                })
                .collect();

            SongDatabase { paths, songs }
        };

        // No point in running without songs.
        assert!(db.len() > 0, "No songs loaded!");

        let new = std::cmp::max(0, db.len() - cache.len());
        log::info!(
            "Loaded {} songs ({} new) in {} seconds",
            db.len(),
            new,
            start_time.elapsed().as_secs()
        );

        if new > 0 {
            // Write out the new database.
            // We don't do this asynchronously because it'll use a bunch of extra memory for
            // serializing vs `serialize_into`.
            let f = BufWriter::new(File::create(DB_PATH).expect("Couldn't open db file for write"));

            bincode::serialize_into(f, &db).expect("Failed to write db");
        }

        db
    }

    pub fn songs(&self) -> &Vec<SongInfo> {
        &self.songs
    }

    pub fn song(&self, index: SongIndex) -> Option<&SongInfo> {
        self.songs.get(index)
    }

    pub fn path(&self, index: SongIndex) -> Option<&String> {
        self.paths.get(&index)
    }

    pub async fn cover_art(&self, index: SongIndex) -> anyhow::Result<CoverArt> {
        // Check if there's cover art before we hit the disk to load it.
        self.song(index)
            .context("Invalid songindex")?
            .cover_art
            .then_some(())
            .context("Song has no cover art")?;

        let path = self.path(index).context("Invalid songindex")?;
        // TODO: This is really slow over NFS, and if we do an async file read we get blocking
        // behaviour on the system because the other NFS reads for playback will be bottlenecked.
        let tag = Tag::read_from_path(path)?;

        // Grab the first cover art we find.
        let cover_art = tag
            .pictures()
            .find(|picture| picture.picture_type == PictureType::CoverFront)
            .context("No front cover found")?;

        Ok(CoverArt {
            data: cover_art.data.clone(),
            mime_type: cover_art.mime_type.clone(),
        })
    }

    pub fn len(&self) -> usize {
        self.songs.len()
    }
}
