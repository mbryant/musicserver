mod client;
mod database;
mod mplayer;
mod server;

use anyhow::Context;
use axum::extract::{Path, State};
use axum::response::{IntoResponse, Response};
use futures_util::{SinkExt, StreamExt};
use serde::Deserialize;
use tokio::net::{TcpListener, TcpStream};
use tokio::sync::{mpsc, RwLock};
use tokio_tungstenite::tungstenite;

#[derive(Deserialize)]
struct ClientQuery {
    sequence: u64,
    command: String,
}

#[tokio::main]
async fn main() {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("debug"))
        .format_timestamp(None)
        .init();

    let (server_queries, server_listen) = mpsc::channel(64);
    let (monitor_update, server_updates) = mpsc::channel(64);
    let (client_notify, clients_list) = mpsc::channel(64);

    // A channel for the cover art thread to be able to share the database with the player.
    let (notify_coverart, mut database_initialized) = mpsc::channel(1);

    // A R/O copy of the song database, used for serving cover art to clients.
    let song_database: SongDatabase = std::sync::Arc::new(tokio::sync::RwLock::new(None));

    // A thread to handle the music player backend.
    // We'll wait wait for it to notify us that it's able to handle requeusts before doing anything
    // interesting.
    tokio::task::spawn_blocking(move || {
        // `server::commands` is an async function, but it mostly does blocking operations
        // internally.  Run it in a different thread to ensure we don't accidentally block other
        // tasks.
        tokio::runtime::Handle::current().block_on(server::commands(
            server_listen,
            monitor_update,
            notify_coverart,
        ))
    });

    // A thread to keep the player up to date (triggering it to play the next song, etc) when no
    // clients are attached.
    tokio::task::spawn(monitor(server_updates, clients_list));

    {
        // A thread to wait for the song database to be ready, so we can serve cover art from Axum.
        let song_database_set = song_database.clone();
        tokio::task::spawn(async move {
            if let Some(song_db) = database_initialized.recv().await {
                song_database_set.write().await.replace(song_db);
            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////
    // Client interactions
    ///////////////////////////////////////////////////////////////////////////
    // The remainder of this function is handling interactions with clients, such as serving the
    // frontend and processing commands.

    // A thread to serve the UI
    tokio::task::spawn(async {
        log::info!("HTTP server accepting connections on [::]:4043");
        axum::serve(
            TcpListener::bind("[::]:4043")
                .await
                .expect("Failed to bind :4043"),
            axum::Router::new()
                .route("/cover_art/{id}", axum::routing::get(cover_art))
                .fallback_service(tower_http::services::ServeDir::new("musicserver/frontend"))
                .with_state(song_database)
                .into_make_service(),
        )
        .await
        .context("Failed to launch app")
    });

    // Continuously handle client connections
    log::info!("Backend accepting connections on :6600");
    let server = TcpListener::bind("[::]:6600")
        .await
        .expect("Could not bind to :6600");

    loop {
        if let Ok((connection, addr)) = server.accept().await {
            log::info!("New connection from {}", addr);

            let (monitor_notifications, server_events) = mpsc::channel(64);
            if client_notify.send(monitor_notifications).await.is_ok() {
                tokio::task::spawn(handle_client(
                    connection,
                    server_queries.clone(),
                    server_events,
                ));
            }
        }
    }
}

/// Handle backend commands for each connected client.
async fn handle_client(
    connection: TcpStream,
    server_query: mpsc::Sender<client::Query>,
    mut server_events: mpsc::Receiver<serde_json::Value>,
) -> anyhow::Result<()> {
    use tungstenite::Message;

    let (mut client_write, mut client_read) =
        tokio_tungstenite::accept_async(connection).await?.split();

    loop {
        tokio::select! {
            client_data = client_read.next() =>
                match client_data.ok_or_else(|| anyhow::anyhow!("Failed to read data from client"))?? {
                    Message::Close(_) => break,
                    Message::Ping(data) =>
                        client_write.send(Message::Pong(data)).await?,
                    Message::Pong(_) => (),
                    Message::Text(json_query) => {
                        let query: ClientQuery = serde_json::from_str(&json_query)?;

                        client::query_server(&query.command, &server_query).await?
                    }
                    unknown => log::error!("Unexpected client data: {}", unknown),
                },
            Some(server_update) = server_events.recv() =>
                client_write.send(Message::text(server_update.to_string())).await?,

            // Ping clients every 30s to keep them awake.
            _ = tokio::time::sleep(std::time::Duration::from_secs(30)) =>
                client_write.send(Message::Ping(axum::body::Bytes::new())).await?
        }
    }

    Ok(())
}

/// Monitor the server, distributing it's state to all of our connected clients.
pub async fn monitor(
    mut server_updates: mpsc::Receiver<serde_json::Value>,
    mut clients: mpsc::Receiver<mpsc::Sender<serde_json::Value>>,
) {
    let mut client_responses: Vec<mpsc::Sender<serde_json::Value>> = Vec::with_capacity(4);
    let mut prev_state = serde_json::json!("initializing");

    loop {
        tokio::select! {
            // A new client registered itself. Mark that we should send to it, and send it the
            // most recent state.
            Some(new_client) = clients.recv() => {
                if new_client.try_send(prev_state.clone()).is_ok() {
                    client_responses.push(new_client);
                }
            },

            Some(new_state) = server_updates.recv() => {
                if !client_responses.is_empty() && prev_state != new_state {
                    log::debug!(
                        "Distributing updated state to up-to {} clients",
                        client_responses.len()
                    );
                    client_responses.retain(|client| client.try_send(new_state.clone()).is_ok());
                    prev_state = new_state;
                }
            },
        }
    }
}

// We want to pass a reference to the song database to Axum, but we need to start Axum before we've
// loaded songs.
// Instead, we give Axum a handle
type SongDatabase = std::sync::Arc<RwLock<Option<&'static database::SongDatabase>>>;

/// Handle client requests for cover art
async fn cover_art(
    Path(song_id): Path<database::SongIndex>,
    State(song_db): State<SongDatabase>,
) -> Result<Response<axum::body::Body>, AxumError> {
    log::debug!("Client requested cover art for song {}", song_id);

    // The song database may not be ready yet, in which case we return a 404.
    let song_db = *song_db.read().await;
    if let Some(song_db) = song_db {
        // The song database is present, return the cover art if we can find it.
        if let Ok(cover_art) = song_db.cover_art(song_id).await {
            return Ok(Response::builder()
                .header("Content-Type", cover_art.mime_type)
                .header("Content-Length", cover_art.data.len())
                .body(axum::body::Body::from(cover_art.data))
                .context("Failed to format successful response")?);
        }
    } else {
        log::warn!("Client requested cover art before the database was ready");
    }

    // We didn't find the requested cover art.
    Ok(Response::builder()
        .status(axum::http::StatusCode::NOT_FOUND)
        .body(axum::body::Body::empty())
        .context("Failed to format error response")?)
}

///////////////////////////////////////////////////////////////////////////////
// Anyhow wrapper
///////////////////////////////////////////////////////////////////////////////
/// To make Anyhow work reasonably with Axum, we need to provide some way for Axum to respond when
/// we give it an [`anyhow::Error`].
struct AxumError(anyhow::Error);

impl IntoResponse for AxumError {
    fn into_response(self) -> Response {
        (
            axum::http::StatusCode::INTERNAL_SERVER_ERROR,
            format!("{:?}", self.0),
        )
            .into_response()
    }
}

impl<E> From<E> for AxumError
where
    E: Into<anyhow::Error>,
{
    fn from(err: E) -> Self {
        Self(err.into())
    }
}
