use anyhow::Context;
use std::fmt::Arguments;
use std::io::{BufRead, BufReader, Write};
use std::process::{self, Stdio};

pub struct Mplayer {
    process: process::Child,
    output: BufReader<process::ChildStdout>,
}

impl Mplayer {
    pub fn new(song: &str) -> anyhow::Result<Mplayer> {
        let mut p = process::Command::new("mplayer")
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::null())
            .args(["-slave", "-quiet", song])
            .spawn()
            .context("Failed to spawn mplayer process")?;

        let stdout = p
            .stdout
            .take()
            .context("Failed to open stdout with process")?;

        Ok(Mplayer {
            process: p,
            output: BufReader::new(stdout),
        })
    }

    pub fn write(&mut self, command: Arguments<'_>) -> anyhow::Result<()> {
        if let Some(ref mut stdin) = self.process.stdin {
            stdin.write_fmt(command)?;
            stdin.write_all("\n".as_bytes())?;
            Ok(())
        } else {
            anyhow::bail!("No stdin for child mplayer")
        }
    }

    /// Returns an iterator over lines on stdout
    pub fn lines(&mut self) -> MplayerOutputLines {
        MplayerOutputLines {
            stdout: &mut self.output,
        }
    }
}

/// A simple iterator over stdout lines.  Ideally we could use [`BufRead::lines()`], but ownership
/// is complicated with that approach due to our usage.
pub struct MplayerOutputLines<'a> {
    stdout: &'a mut BufReader<process::ChildStdout>,
}

impl Iterator for MplayerOutputLines<'_> {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        let mut buffer = String::new();

        self.stdout.read_line(&mut buffer).ok()?;

        // NOTE: both mplayer and mpg123 decoding specific.
        (!buffer.starts_with("Exiting") && !buffer.starts_with("mpg123")).then_some(buffer)
    }
}

impl Drop for Mplayer {
    fn drop(&mut self) {
        let _ = self.process.kill();
        // Remember to reap the process
        let _ = self.process.try_wait();
    }
}
