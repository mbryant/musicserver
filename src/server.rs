use anyhow::Context;
use nanorand::Rng;
use serde::Serialize;
use std::{mem, process::Command};
use tokio::sync::mpsc;
use tokio::time;

use crate::client::Query;
use crate::database::{SongDatabase, SongIndex};
use crate::mplayer::Mplayer;

#[derive(Serialize)]
struct Song {
    playing: bool,
    position: u32,
    duration: u32,
    song: SongIndex,
    #[serde(skip_serializing)]
    mplayer: Mplayer,
}

#[derive(Serialize)]
struct State<'db> {
    /// The player's volume, from 0-100
    volume: u32,

    /// The queue of next songs to be played.
    ///
    /// NOTE: The queue needs to be manually created, so we never see long enough queues to make it
    /// worth representing this as a VeqDequeue instead.
    queue: Vec<SongIndex>,

    /// The currently playing song, if it exists
    playback: Option<Song>,

    /// The database of loaded songs.
    #[serde(skip_serializing)]
    db: &'db SongDatabase,

    // TODO: we'd like our own PRNG instead, as we don't really want songs to be repeated until
    // they've all played, and we want to bias towards artists that haven't been played recently.
    #[serde(skip)]
    rng: nanorand::WyRand,
}

impl<'db> State<'db> {
    pub fn new(db: &'db SongDatabase) -> Self {
        Self {
            volume: 100,
            queue: Vec::new(),
            playback: None,
            db,
            rng: nanorand::WyRand::new(),
        }
    }

    pub fn songlist(&self) -> anyhow::Result<serde_json::Value> {
        Ok(serde_json::to_value(self.db.songs())?)
    }

    fn enqueue(&mut self, song: SongIndex) -> anyhow::Result<()> {
        self.queue.push(song);
        Ok(())
    }

    fn clear_queue(&mut self) -> anyhow::Result<()> {
        self.queue.clear();
        Ok(())
    }

    fn load_song(&mut self, id: SongIndex) -> anyhow::Result<()> {
        self.playback = None;

        let song = self.db.song(id).context("No song found for given ID")?;
        let path = self.db.path(id).expect("ID was already validated by song");

        log::info!("Playing song {}", path);

        let mut mplayer = Mplayer::new(path)?;
        if song.duration == 0 {
            // We don't have the length for this song, so query for it.  The first update
            // will handle the response.
            // TODO: It might be nice to query duration before the song starts.
            mplayer.write(format_args!("get_property length"))?
        }

        self.playback = Some(Song {
            playing: true,
            position: 0,
            duration: 0,
            song: id,
            mplayer,
        });

        Ok(())
    }

    // Play the next song from the queue, or select a random song if nothing has been specified.
    fn next_song(&mut self) -> anyhow::Result<()> {
        let id = if self.queue.is_empty() {
            self.rng.generate_range(0..self.db.len())
        } else {
            self.queue.remove(0)
        };

        self.load_song(id)
    }

    fn pause(&mut self) -> anyhow::Result<()> {
        if let Some(ref mut playback) = self.playback {
            if playback.playing {
                playback.mplayer.write(format_args!("pause"))?;
                playback.playing = false;
            }
        }

        Ok(())
    }

    fn resume(&mut self) -> anyhow::Result<()> {
        if let Some(ref mut playback) = self.playback {
            if !playback.playing {
                playback.mplayer.write(format_args!("pause"))?;
                playback.playing = true;
            }
        }

        Ok(())
    }

    fn position(&mut self, time: u32) -> anyhow::Result<()> {
        if let Some(ref mut playback) = self.playback {
            playback.mplayer.write(format_args!("seek {} 2", time))?;
            playback.position = time;
        } else {
            anyhow::bail!("No song playing")
        }

        Ok(())
    }

    fn update(&mut self) -> anyhow::Result<()> {
        if let Some(ref mut playback) = self.playback {
            // We must ensure the song is currently playing when doing an update, as the device
            // doesn't deal well with position queries when playback is paused.
            if !playback.playing {
                return Ok(());
            }

            // Send a position query, then search through mplayer's logs until we see the response.
            // TODO: Replace with libmpv
            playback.mplayer.write(format_args!("get_time_pos"))?;

            for line in playback.mplayer.lines() {
                let line = line.trim();

                if let Some(num) = line.strip_prefix("ANS_TIME_POSITION=") {
                    // NOTE: This was originally done with a simple parse to f32, which
                    // handles the decimal properly.  However, this Pi appears to have
                    // problems with parsing correctly, leading us to use an int instead.
                    // As we poll at least every second, the loss of precision is not
                    // noticed.  Additionally, mplayer has sometimes been noticed to give
                    // small negative numbers, which cause parsing to fail.  We convert
                    // these to 0.
                    let seconds: u32 = (match num.find('.') {
                        Some(i) => num[0..i].parse(),
                        None => num.parse(),
                    })
                    .unwrap_or(0);

                    playback.position = seconds;
                    break;
                } else if let Some(num) = line.strip_prefix("ANS_length=") {
                    let seconds: u32 = (match num.find('.') {
                        Some(i) => num[0..i].parse(),
                        None => num.parse(),
                    })
                    .unwrap_or(0);

                    // Record the client.
                    playback.duration = seconds;
                } else {
                    log::debug!("[mplayer] {}", line);
                }
            }
        }

        Ok(())
    }

    fn volume(&mut self, volume: u32) -> anyhow::Result<()> {
        // We only accept volumes as a percent.
        if volume <= 100 {
            Command::new("amixer")
                .args(["-q", "-M", "sset", "Master", &format!("{}%", volume)])
                .spawn()?;

            // TODO: Won't be right after a restart or if changed by another process.
            // We need a way to reread this.
            self.volume = volume;
        }

        Ok(())
    }
}

/// Handle commands sent by the main thread on behalf of clients.
///   receive: a channel with commands to execute
///   monitor_update: a channel to notify the monitor thread whenever there's an update to our
///     state (mostly due to commands being run).
pub async fn commands(
    mut receive: mpsc::Receiver<Query>,
    monitor_update: mpsc::Sender<serde_json::Value>,
    notify_coverart: mpsc::Sender<&SongDatabase>,
) -> ! {
    // Load the list of songs and initialize the mplayer infrastructure.
    // NOTE: this may take an exceedingly long time on the pi (30+ mins) during an uncached load,
    // so we thread once we've loaded everything and are ready to handle requests.  Most of the
    // overhead appears to come from loading hundreds of GB over NFS, which will probably never be
    // quick.
    let db = Box::leak(Box::new(SongDatabase::new().await));
    let mut state = State::new(db);
    monitor_update
        .send(serde_json::to_value(&state).expect("Initial state was invalid"))
        .await
        .expect("Monitor thread crashed?");
    notify_coverart
        .send(db)
        .await
        .expect("HTTP cover art thread crashed");

    loop {
        let command = tokio::select! {
            client_command = receive.recv() => {
                // We received a command from one of our clients
                let command = client_command.expect("Something weird happened with the client parser");
                log::debug!("Server query: {:?}", command);

                command
            }
            _ = time::sleep(time::Duration::from_millis(330)) => {
                // We should periodically refresh ourselves
                Query::InternalUpdate
            }
        };

        let response = if matches!(command, Query::SongList) {
            // The `songlist` query is an outlier, as it's the only one that we'll specifically
            // send out something other than our state for.
            state.songlist()
        } else {
            let mplayer_status = match command {
                Query::Load(song) => state.load_song(song),
                Query::Position(p) => state.position(p),
                Query::Enqueue(song) => state.enqueue(song),
                Query::Volume(percent) => state.volume(percent),
                Query::ClearQueue => state.clear_queue(),
                Query::Next => state.next_song(),
                Query::Pause => state.pause(),
                Query::Resume => state.resume(),
                Query::InternalUpdate => state.update(),
                Query::SongList => unreachable!(),
            };

            mplayer_status.map(|_| serde_json::to_value(&state).expect("Failed to serialize state"))
        };

        match response {
            Ok(response) => {
                monitor_update
                    .send(response)
                    .await
                    .expect("Monitor thread died?");
                continue;
            }
            Err(response) => log::error!("Query failed: {}", response),
        }

        // If we reached here, then something failed, most likely because mplayer exited, either
        // because the song ended or because of a crash.  We need to ensure the current playback
        // is stopped to keep our state healthy.
        //
        // As we're stopping the current playback state, we take ownership of it to do
        // cleanup.
        let mut playback = None;
        mem::swap(&mut playback, &mut state.playback);

        // If the song ended (playback = None), we must have been playing a song, so we should play
        // the next song.  The worst case here is that the user's interaction with the player is
        // dropped - we don't lose any data.
        // If the song didn't end, then we need to maintain the playing status - if mplayer crashes
        // while paused, we can't suddenly play music!  We should play the next song if it was
        // currently playing, and do nothing otherwise.
        if !playback.map_or(false, |x| !x.playing) {
            let _ = state.next_song();
        }

        monitor_update
            .send(serde_json::to_value(&state).expect("Server state was invalid"))
            .await
            .expect("Monitor thread died?");
    }
}
